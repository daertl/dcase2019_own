#!/bin/bash

set -e

EVAL_NAME=test
TSV_FILE="$EVAL_NAME.tsv"
DCASE=/home/stud/f/fabunt21/DCASE2019_task4

usage() {
    printf "usage:\n  ${0##*/} FILES..\n"
    echo "  analyze sound event files"
}

[ -z "$1" ] && {
    printf "error: provide files as arguments\n\n"
    usage	
    exit
}

cd "$DCASE"

# activate conda environment
source /opt/anaconda3/etc/profile.d/conda.sh
conda activate ./env

mkdir -p "$EVAL_NAME"

# create input tsv file
cat <<- EOF > "$TSV_FILE"
filename	onset	offset	event_label
EOF

for f; do
    [ -f "$f" ] || continue
    echo "${f#*/}" >> "$TSV_FILE"

    # move audio file into correct dir
    [ -f "$EVAL_NAME/${f#*/}" ] || mv "$f" "$EVAL_NAME"
done

cd "baseline"

# predict sound events from file
python UseModel.py -f "$EVAL_NAME.tsv" -m="stored_data/MeanTeacher_with_synthetic/model/baseline_best"

# show predictions
printf "\n\nPREDICITONS:\n"
cat predictions.tsv
